#include <Windows.h>
#include <winres.h>
#include "resource.h"

//----------------------------------------------------------------
// build a file from resource
//----------------------------------------------------------------
bool decompress_sysfile(DWORD RESOURCE, LPCWSTR filename)
{
	HRSRC aResourceH;
	HGLOBAL aResourceHGlobal;
	unsigned char * aFilePtr;
	unsigned long aFileSize;
	HANDLE file_handle;
	// IDR_RCDATA1
	aResourceH = FindResource(NULL, MAKEINTRESOURCE(RESOURCE), RT_RCDATA);
	if (!aResourceH)
	{
		return false;
	}

	aResourceHGlobal = LoadResource(NULL, aResourceH);
	if (!aResourceHGlobal)
	{
		return false;
	}

	aFileSize = SizeofResource(NULL, aResourceH);
	aFilePtr = (unsigned char *)LockResource(aResourceHGlobal);
	if (!aFilePtr)
	{
		return false;
	}

	file_handle =
		CreateFileW(
			filename,
			FILE_ALL_ACCESS,
			0,
			NULL,
			CREATE_ALWAYS,
			0,
			NULL);

	if (INVALID_HANDLE_VALUE == file_handle)
	{
		return false;
	}

};

int main()
{
	MessageBoxW(NULL, L"Content", L"Header", MB_OK);
	// Pass the arguments of resource and output path
	decompress_sysfile(IDR_RCDATA1, L"C:\Hello.exe");
	ExitProcess(NULL);

}